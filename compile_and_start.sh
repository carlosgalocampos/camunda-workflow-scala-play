#!/usr/bin/env bash

# parse command-line arguments
while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help)
			arg_h=true
			;;
		-z|--zkhost)
			arg_z="$2"
			shift
			;;
		*)
			# unknown option
			;;
	esac
	shift
done

# print help
if [ "$arg_h" = "true" ]; then
	echo ""
	echo "Usage:"
	echo ""
	echo "  ./$(basename $0) [OPTIONS]"
	echo ""
	echo "Options:"
	echo ""
	echo "  -h, --help            Print this message"
	echo "  -z, --zkhost <arg>    Set the ZOOKEEPER_QUORUM environment variable"
	echo ""
	exit 0
fi

# export zkhost info for the lingotek configurator jar
export ZOOKEEPER_QUORUM="$arg_z"

pid_files="
target/universal/stage/RUNNING_PID
/tmp/RUNNING_PID
"

sbt compile stage

if [ $? -eq 0 ]; then
	# remove any PID files from previous runs
	for i in $pid_files; do
		if [ -f "$i" ]; then
			rm -f "$i"
		fi
	done

	# find the name of the bin file
	bin_file="$(ls -1 target/universal/stage/bin/ | grep -v "\.bat" | head -n 1)"

	# generate a random play crypto secret
	play_crypto_secret="$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32)"

	# start in production mode
	/bin/bash "target/universal/stage/bin/$bin_file" /tmp JAVA_OPTS "-Dplay.crypto.secret=\"$play_crypto_secret\""
fi
