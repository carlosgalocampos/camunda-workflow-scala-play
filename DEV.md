# Development

## Build

To compile the code:  
`sbt clean compile`

To build a single executable package:  
`sbt dist`

This will generate a zip file in `target/universal`.  
Extract the zip and go to the bin directory.

The `start` command for the play sbt plugin has been deprecated.  A bash script has been created to compile the code, create a staged version of the dist, and then run it.  Pass zookeper host as the `-z` parameter:  
`./compile_and_start.sh -z "172.28.128.8:2181"`

Use the `-h` option to print the help information of that script:  
`./compile_and_start.sh -h`

To run from checked out code using sbt with JIT compilation instead of the dist:  
`sbt run`
