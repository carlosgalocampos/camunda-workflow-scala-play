import sbt.Keys._
import sbt._
import scala.xml.XML


name := "lingotek-middleware-workflow-api"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

val mavenSettingsPath = s"${Path.userHome.absolutePath}/.m2/settings.xml"
val artifactoryHost = "http://10.0.11.14:8081"

credentials ++= {
  (XML.loadFile(new File(mavenSettingsPath)) \ "servers" \ "server").map { s =>
    Credentials("artifactory", (s \ "id").text, (s \ "username").text, (s \ "password").text)
  }
}

resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("public"),
  Resolver.typesafeIvyRepo("releases"),
  "Lingotek Artifactory Master" at s"${artifactoryHost}/artifactory/master-release-local",
  "Lingotek Artifactory Dev" at s"${artifactoryHost}/artifactory/dev-release-local"
)

libraryDependencies ++= Seq(
  specs2 % Test,
  evolutions,
  "com.lingotek" % "configurator_2.11" % "0.1.2",
  //  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "com.typesafe.play" %% "play-slick" % "2.0.2",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.2",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.1.1",
  "org.postgresql" % "postgresql" % "9.3-1104-jdbc41",
  "com.h2database" % "h2" % "1.4.190",
  "org.camunda.bpm" % "camunda-bom" % "7.4.0",
  "org.camunda.bpm" % "camunda-engine" % "7.4.0"
)

fork in Test := false

lazy val root = (project in file(".")).enablePlugins(PlayScala)