# API Documentation

## Summary

This system provides REST APIs for you to use.  All response content is JSON.  Every path starts with the Base Path listed below.

**Base Path**  
/v1

**Requests**  

|Method|Path|Summary|
|:---|:---|:---|
|GET|/event|List the events for the given object.|
|POST|/event|Add a new event to the given object.|
|DELETE|/event/`event_id`|Remove the event.|
|GET|/event/`event_id`|Get the details of the event.|
|PATCH|/event/`event_id`|Edit the event.|
|GET|/file/`file_id`/count/locale/pair|List all locale pairs and their total counts of TM units in the file.|
|GET|/file/`file_id`/count/tmunit|Get the total count of all TM units in the file.|
|DELETE|/flag|Remove all flags on the TM unit.|
|GET|/flag|List the flags.|
|POST|/flag|Flag a TM unit.  The Public Vault is not supported.|
|DELETE|/flag/`flag_id`|Remove the flag.|
|GET|/flag/`flag_id`|Get the details of the flag.|
|PATCH|/flag/`flag_id`|Edit the flag.|
|DELETE|/mt/training|Remove all recorded MT trainings from the TM unit.|
|GET|/mt/training|List the MT trainings for the TM unit.|
|POST|/mt/training|Add a new MT training for the TM unit.  This is just historical logging data.|
|DELETE|/mt/training/`training_id`|Remove the MT training.|
|GET|/mt/training/`training_id`|Get the details of the MT training.|
|PATCH|/mt/training/`training_id`|Edit the MT training.|
|DELETE|/rating|Remove all ratings on the TM unit.|
|GET|/rating|List the ratings.|
|POST|/rating|Rate a TM unit.  The Public Vault is not supported.|
|DELETE|/rating/`rating_id`|Remove the rating.|
|GET|/rating/`rating_id`|Get the details of the rating.|
|PATCH|/rating/`rating_id`|Edit the rating.|
|POST|/search/concordance|Perform a TM search based on concordance.|
|POST|/search/distance|Perform a TM search based on edit-distance.|
|POST|/search/index|Perform a simple search against the index of TM units.|
|GET|/status|Get the status of the system.|
|POST|/status|Change the status of the system.|
|POST|/tmunit|Add a new TM unit.|
|GET|/tmunit/listing|Generate a list of TM unit IDs.|
|DELETE|/tmunit/`unit_id`|Remove the TM unit and all related data such as events, flags, etc.|
|GET|/tmunit/`unit_id`|Get the details of the TM unit.|
|PATCH|/tmunit/`unit_id`|Edit the TM unit.|
|DELETE|/tmunit/`unit_id`/field/map|Remove all keys and values from the given map collection field.|
|PATCH|/tmunit/`unit_id`/field/map|Set the value for the key in the given map collection field.|
|DELETE|/tmunit/`unit_id`/field/set|Remove a single or all values from the given set collection field.|
|POST|/tmunit/`unit_id`/field/set|Add a new value to the given set collection field.|
|POST|/tmunit/`unit_id`/index|Reindex the TM unit.|
|POST|/tmunit/`unit_id`/move|Move the TM unit to a different vault.|
|GET|/vault/`vault_id`/count/locale/pair|List all locale pairs and their total counts of TM units in the vault.|
|GET|/vault/`vault_id`/count/tmunit|Get the total count of all TM units in the vault.|

## Paths

### GET /event

**Summary**  
List the events for the given object.  They will always be ordered by created timestamp in descending order.  Currently this is only intended to be used for individual objects like TM units because all events for that object will be stored on the same partition in cassandra.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|created_end|query|Filter the list by those created on or before this timestamp.|false|integer (int64)|
|created_start|query|Filter the list by those created on or after this timestamp.|false|integer (int64)|
|event_type|query|Filter the list by event type.|false|string|
|object_id|query|ID of the object.|true|string (uuid)|
|user_id|query|Filter the list by user ID.|false|string (uuid)|

**Responses**  
200  

### POST /event

**Summary**  
Add a new event to the given object.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|event_data|formData|Data for the event.  This must be a valid JSON object stored as a single String.  It cannot be a JSON array and is always parsed as a single JSON object.|false|string|
|event_type|formData|Type of the event.|true|string|
|object_id|formData|ID of the object.|true|string (uuid)|
|user_id|formData|ID of the user.|false|string (uuid)|

**Responses**  
201  

### DELETE /event/`event_id`

**Summary**  
Remove the event.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`event_id`|path|ID of the event.|true|string (uuid)|

**Responses**  
204  

### GET /event/`event_id`

**Summary**  
Get the details of the event.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`event_id`|path|ID of the event.|true|string (uuid)|

**Responses**  
200  

### PATCH /event/`event_id`

**Summary**  
Edit the event.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|event_data|formData|Data for the event.  Often a JSON object stored as a String.|false|string|
|`event_id`|path|ID of the event.|true|string (uuid)|
|event_type|formData|Type of the event.|false|string|
|user_id|formData|ID of the user.|false|string (uuid)|

**Responses**  
204  

### GET /file/`file_id`/count/locale/pair

**Summary**  
List all locale pairs and their total counts of TM units in the file.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`file_id`|path|ID of the file.|true|string (uuid)|

**Responses**  
200  

### GET /file/`file_id`/count/tmunit

**Summary**  
Get the total count of all TM units in the file.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`file_id`|path|ID of the file.|true|string (uuid)|

**Responses**  
200  

### DELETE /flag

**Summary**  
Remove all flags on the TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|query|ID of the TM unit.|true|string (uuid)|

**Responses**  
204  

### GET /flag

**Summary**  
List the flags.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|query|ID of the TM unit.|true|string (uuid)|

**Responses**  
200  

### POST /flag

**Summary**  
Flag a TM unit.  The Public Vault is not supported.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|formData|ID of the TM unit.|true|string (uuid)|
|user_id|formData|ID of the user.|false|string (uuid)|

**Responses**  
201  

### DELETE /flag/`flag_id`

**Summary**  
Remove the flag.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`flag_id`|path|ID of the flag.|true|string (uuid)|

**Responses**  
204  

### GET /flag/`flag_id`

**Summary**  
Get the details of the flag.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`flag_id`|path|ID of the flag.|true|string (uuid)|

**Responses**  
200  

### PATCH /flag/`flag_id`

**Summary**  
Edit the flag.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`flag_id`|path|ID of the flag.|true|string (uuid)|
|user_id|formData|ID of the user.|false|string (uuid)|

**Responses**  
204  

### DELETE /mt/training

**Summary**  
Remove all recorded MT trainings from the TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|query|ID of the unit.|true|string (uuid)|

**Responses**  
204  

### GET /mt/training

**Summary**  
List the MT trainings for the TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|query|ID of the unit.|true|string (uuid)|

**Responses**  
200  

### POST /mt/training

**Summary**  
Add a new MT training for the TM unit.  This is just historical logging data.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|category|formData|Category used with the MT engine.|false|string|
|engine_id|formData|ID of the MT engine.|true|string (uuid)|
|unit_id|query|ID of the unit.|true|string (uuid)|

**Responses**  
201  

### DELETE /mt/training/`training_id`

**Summary**  
Remove the MT training.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`training_id`|path|ID of the training.|true|string (uuid)|

**Responses**  
204  

### GET /mt/training/`training_id`

**Summary**  
Get the details of the MT training.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`training_id`|path|ID of the training.|true|string (uuid)|

**Responses**  
200  

### PATCH /mt/training/`training_id`

**Summary**  
Edit the MT training.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|category|formData|Category used with the MT engine.|false|string|
|engine_id|formData|ID of the MT engine.|false|string (uuid)|
|unit_id|query|ID of the unit.|false|string (uuid)|

**Responses**  
204  

### DELETE /rating

**Summary**  
Remove all ratings on the TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|query|ID of the TM unit.|true|string (uuid)|

**Responses**  
204  

### GET /rating

**Summary**  
List the ratings.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|unit_id|query|ID of the TM unit.|false|string (uuid)|

**Responses**  
200  

### POST /rating

**Summary**  
Rate a TM unit.  The Public Vault is not supported.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|rating_value|formData|Decimal rating out of 5 stars.|true|number (float)|
|unit_id|formData|ID of the TM unit.|true|string (uuid)|
|user_id|formData|ID of the user.|false|string (uuid)|

**Responses**  
201  

### DELETE /rating/`rating_id`

**Summary**  
Remove the rating.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`rating_id`|path|ID of the rating.|true|string (uuid)|

**Responses**  
204  

### GET /rating/`rating_id`

**Summary**  
Get the details of the rating.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`rating_id`|path|ID of the rating.|true|string (uuid)|

**Responses**  
200  

### PATCH /rating/`rating_id`

**Summary**  
Edit the rating.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`rating_id`|path|ID of the rating.|true|string (uuid)|
|rating_value|formData|Decimal rating out of 5 stars.|false|number (float)|
|unit_id|formData|ID of the TM unit.|false|string (uuid)|
|user_id|formData|ID of the user.|false|string (uuid)|

**Responses**  
204  

### POST /search/concordance

**Summary**  
Perform a TM search based on concordance.  It is used to find TM units which are similar to or related to the source.  It performs a TM search based on similarity of text and provides highlighting of matching text in the results.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|filter_compare_type|formData|Type of comparison to perform for this filter.  An example would be "equals".  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each filter.  See Appendix section for more information.|false|string|
|filter_compare_value|formData|Value of the comparison for this filter.  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each filter.|false|string|
|filter_duplicates|formData|Whether or not to filter duplicate matches.  A duplicate is when the source and target text (including formatting) is identical and the match strength is identical.  Metadata is not considered.  There is no guarantee on which duplicate is removed (i.e. assume it will be random).  The default is true and to filter them.|false|boolean|
|filter_field|formData|Name of a field on the TM units to filter the matches by.  An example would be "project_id".  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each filter.  However, the overall order of how filters are applied does not matter.|false|string|
|limit|formData|Count of matches to limit the results by.  The default is 10.  The max is 50.|false|integer (int32)|
|search_reverse|formData|Whether or not to search for reverse matches.  The default is false.|false|boolean|
|sort_direction|formData|Direction to apply this sort.  The value can either be "asc" or "desc".|false|string|
|sort_field|formData|Name of a field on the TM units to sort the matches by.    This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each sort and the order in which they are applied to the overall cascaded sorting of matches.|false|string|
|source_country|formData|ISO 3166 country code for the source.  An example would be "US".|false|string|
|source_language|formData|ISO 639 langauge code for the source.  An example would be "en".|true|string|
|source_text|formData|Plain-text of the source.|true|string|
|target_country|formData|ISO 3166 country code for the target.  An example would be "BR".|false|string|
|target_language|formData|ISO 639 langauge code for the target.  An example would be "pt".|true|string|
|vault_id|formData|ID of the vault.  This parameter can have multiple values.  The order in which they are passed determines the vault priority.|true|string (uuid)|

**Responses**  
200  

### POST /search/distance

**Summary**  
This is the main algorithm for searching.  It is used to find TM units which match as closely as possible in order to reduce effort and cost.  It performs a TM search based on [edit distance](https://en.wikipedia.org/wiki/Edit_distance).  The chosen metric for calculating edit distance is the [Levenshtein distance](https://en.wikipedia.org/wiki/Levenshtein_distance) and is calculated for fuzzy matches by solr's [strdist](https://wiki.apache.org/solr/FunctionQuery#strdist) function.

If no custom sorting is specified, the default sorting will be: match type, modified score, vault priority, then last modified timestamp.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|filter_compare_type|formData|Type of comparison to perform for this filter.  An example would be "equals".  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each filter.  See Appendix section for more information.|false|string|
|filter_compare_value|formData|Value of the comparison for this filter.  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each filter.|false|string|
|filter_duplicates|formData|Whether or not to filter duplicate matches.  A duplicate is when the source and target text (including formatting) is identical and the match strength is identical.  Metadata is not considered.  There is no guarantee on which duplicate is removed (i.e. assume it will be random).  The default is true and to filter them.|false|boolean|
|filter_field|formData|Name of a field on the TM units to filter the matches by.  An example would be "project_id".  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each filter.  However, the overall order of how filters are applied does not matter.|false|string|
|limit|formData|Count of matches to limit the results by.  The default is 10.  The max is 50.|false|integer (int32)|
|modifier_change|formData|Decimal value to modify the match strength by.|false|string|
|modifier_compare_type|formData|Type of comparison to perform for this modifier.  An example would be "equals".  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each modifier.  See Appendix section for more information.|false|string|
|modifier_compare_value|formData|Value of the comparison for this modifier.  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each modifier.|false|string|
|modifier_field|formData|Name of a field on the TM units to modify the matches based on.  An example would be "project_id".  This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each modifier.  However, the overall order of how modifiers are applied does not matter.|false|string|
|penalize_alpha|formData|Decimal value to modify the match strength by when penalizing alpha characters.  The default is 0.1 for each alpha character difference.|false|number (float)|
|penalize_nonalpha|formData|Decimal value to modify the match strength by when penalizing non-alpha characters.  The default is 0.5 for each nonalpha character difference.|false|number (float)|
|penalize_tags|formData|Decimal value to modify the match strength by when penalizing tags.  The default is 0.25 for each format tag.|false|number (float)|
|penalize_variations|formData|Decimal value to modify the match strength by when penalizing variations in translations.  By default this is not applied.|false|number (float)|
|search_all_types|formData|Whether or not to continue searching for matches of lower strengths.  An example would be "true" to search for fuzzy matches even when exact matches were found.  This should not be used when doing bulk searching across many source segments due to optimize performance.|false|boolean|
|search_reverse|formData|Whether or not to search for reverse matches.  The default is false.|false|boolean|
|sort_direction|formData|Direction to apply this sort.  The value can either be "asc" or "desc".|false|string|
|sort_field|formData|Name of a field on the TM units to sort the matches by.    This parameter can have multiple values.  The order in which they are passed determines the grouping of parameters for each sort and the order in which they are applied to the overall cascaded sorting of matches.|false|string|
|source_country|formData|ISO 3166 country code for the source.  An example would be "US".|false|string|
|source_formatting|formData|JSON array of the formatted source represented as a String.|false|string|
|source_language|formData|ISO 639 langauge code for the source.  An example would be "en".|true|string|
|source_next_text|formData|Plain-text of the next source used to specify context.|false|string|
|source_prev_text|formData|Plain-text of the previous source used to specify context.|false|string|
|source_text|formData|Plain-text of the source.|true|string|
|string_id|formData|String ID used to specify context.|false|string|
|target_country|formData|ISO 3166 country code for the target.  An example would be "BR".|false|string|
|target_language|formData|ISO 639 langauge code for the target.  An example would be "pt".|true|string|
|vault_id|formData|ID of the vault.  This parameter can have multiple values.  The order in which they are passed determines the vault priority.|true|string (uuid)|

**Responses**  
200  

### POST /search/index

**Summary**  
Perform a simple search against the index of TM units.  This search provides paginated results.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|created_end|query|Filter the list by those created on or before this timestamp.|false|integer (int64)|
|created_start|query|Filter the list by those created on or after this timestamp.|false|integer (int64)|
|limit|formData|Count of matches to limit the results by.  The default is 10.|false|integer (int32)|
|modified_end|query|Filter the list by those modified on or before this timestamp.|false|integer (int64)|
|modified_start|query|Filter the list by those modified on or after this timestamp.|false|integer (int64)|
|offset|formData|Number to offset results by.  The default is 0.|false|integer (int32)|
|sort_direction|formData|Direction to sort matches by.  The value can either be "asc" or "desc".  The default is "asc".|false|string|
|sort_field|formData|Name of a field on the TM units in the index to sort the matches by.|false|string|
|source_country|formData|ISO 3166 country code for the source.  An example would be "US".|false|string|
|source_language|formData|ISO 639 langauge code for the source.  An example would be "en".|true|string|
|source_text|formData|Plain-text of the source.|false|string|
|target_country|formData|ISO 3166 country code for the target.  An example would be "BR".|false|string|
|target_language|formData|ISO 639 langauge code for the target.  An example would be "pt".|true|string|
|target_text|formData|Plain-text of the target.|false|string|
|vault_id|formData|ID of the vault.  This parameter can have multiple values.  The order in which they are passed determines the vault priority.|true|string (uuid)|

**Responses**  
200  

### GET /status

**Summary**  
Get the status of the system.

**Parameters**  
None.

**Responses**  
200  
503  

### POST /status

**Summary**  
Change the status of the system.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|status_code|formData|Status to use for the system.|true|integer (int32)|

**Responses**  
200  

### POST /tmunit

**Summary**  
Add a new TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|batch_id|formData|ID of the batch.|false|string (uuid)|
|categories|formData|Category text.  This parameter can have multiple values.|false|string|
|classification_levels|formData|Classification level.  This parameter can have multiple values.|false|string|
|community_id|formData|ID of the community.|false|string (uuid)|
|custom_field_types|formData|Type of this custom field.  This parameter can have multiple values. The order in which they are passed determines the grouping of parameters for each custom field.|false|string|
|custom_field_values|formData|Value of this custom field.  This parameter can have multiple values. The order in which they are passed determines the grouping of parameters for each custom field.|false|string|
|custom_field|formData|Name of a custom field.  This parameter can have multiple values. The order in which they are passed determines the grouping of parameters for each custom field.|false|string|
|customers_client_id|formData|ID of the client of one of our customers.|false|string (uuid)|
|document_id|formData|ID of the document.|false|string (uuid)|
|event_user_id|formData|ID of the user adding the TM unit.|false|string (uuid)|
|external_link|formData|Link to an external resource.|false|string|
|file_id|formData|ID of the file.|false|string (uuid)|
|image_path|formData|Path of the image.|false|string|
|job_id|formData|ID of the job.|false|string (uuid)|
|paragraph_id|formData|ID of the paragraph.|false|string (uuid)|
|phase_id|formData|ID of the phase.|false|string (uuid)|
|project_id|formData|ID of the project.|false|string (uuid)|
|segment_id|formData|ID of the segment.|false|string (uuid)|
|source_country|formData|ISO 3166 country code for the source.  An example would be "US".|false|string|
|source_formatting|formData|JSON array of the formatted source represented as a String.|false|string|
|source_language|formData|ISO 639 langauge code for the source.  An example would be "en".|true|string|
|source_next_text|formData|Plain-text of the next source used to specify context.|false|string|
|source_prev_text|formData|Plain-text of the previous source used to specify context.|false|string|
|source_text|formData|Plain-text of the source.|true|string|
|status|formData|Status of the unit.|false|string|
|string_id|formData|String ID used to specify context.|false|string|
|target_country|formData|ISO 3166 country code for the target.  An example would be "BR".|false|string|
|target_formatting|formData|JSON array of the formatted target represented as a String.|false|string|
|target_language|formData|ISO 639 langauge code for the target.  An example would be "pt".|true|string|
|target_text|formData|Plain-text of the target.|true|string|
|task_id|formData|ID of the task.|false|string (uuid)|
|user_id|formData|ID of the user who created this unit.|false|string (uuid)|
|vault_id|formData|ID of the task.|true|string (uuid)|
|vendor_id|formData|ID of the task.|false|string (uuid)|

**Responses**  
201  

### GET /tmunit/listing

**Summary**  
Generate a list of TM unit IDs.  At least one of the parameters listed below must be passed.  However, the parameters are mutually exclusive, meaning they should not be mixed.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|file_id|query|ID of the file.|false|string (uuid)|
|segment_id|query|ID of the segment.|false|string (uuid)|
|vault_id|query|ID of the vault.|false|string (uuid)|

**Responses**  
200  

### DELETE /tmunit/`unit_id`

**Summary**  
Remove the TM unit and all related data such as events, flags, etc.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
204  

### GET /tmunit/`unit_id`

**Summary**  
Get the details of the TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
200  

### PATCH /tmunit/`unit_id`

**Summary**  
Edit the TM unit.  As a note, this call cannot be used to move a TM unit to another vault.  The API for moving TM units must be used in that scenario.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|batch_id|formData|ID of the batch.|false|string (uuid)|
|community_id|formData|ID of the community.|false|string (uuid)|
|customers_client_id|formData|ID of the client of one of our customers.|false|string (uuid)|
|document_id|formData|ID of the document.|false|string (uuid)|
|event_user_id|formData|ID of the user editing the TM unit.|false|string (uuid)|
|external_link|formData|Link to an external resource.|false|string|
|file_id|formData|ID of the file.|false|string (uuid)|
|image_path|formData|Path of the image.|false|string|
|job_id|formData|ID of the job.|false|string (uuid)|
|paragraph_id|formData|ID of the paragraph.|false|string (uuid)|
|phase_id|formData|ID of the phase.|false|string (uuid)|
|project_id|formData|ID of the project.|false|string (uuid)|
|segment_id|formData|ID of the segment.|false|string (uuid)|
|source_country|formData|ISO 3166 country code for the source.  An example would be "US".|false|string|
|source_formatting|formData|JSON array of the formatted source represented as a String.|false|string|
|source_language|formData|ISO 639 langauge code for the source.  An example would be "en".|true|string|
|source_next_text|formData|Plain-text of the next source used to specify context.|false|string|
|source_prev_text|formData|Plain-text of the previous source used to specify context.|false|string|
|source_text|formData|Plain-text of the source.|true|string|
|status|formData|Status of the unit.|false|string|
|string_id|formData|String ID used to specify context.|false|string|
|target_country|formData|ISO 3166 country code for the target.  An example would be "BR".|false|string|
|target_formatting|formData|JSON array of the formatted target represented as a String.|false|string|
|target_language|formData|ISO 639 langauge code for the target.  An example would be "pt".|true|string|
|target_text|formData|Plain-text of the target.|true|string|
|task_id|formData|ID of the task.|false|string (uuid)|
|`unit_id`|path|ID of the unit.|true|string (uuid)|
|user_id|formData|ID of the user who modified this unit.|false|string (uuid)|
|vendor_id|formData|ID of the task.|false|string (uuid)|

**Responses**  
204  

### DELETE /tmunit/`unit_id`/field/map

**Summary**  
Remove all keys and values from the given map collection field.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|field|formData|Name of the map collection field.|true|string|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
204  

### PATCH /tmunit/`unit_id`/field/map

**Summary**  
Set the value for the key in the given map collection field.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|field|formData|Name of the map collection field.|true|string|
|field_key|formData|Key of the map collection field.|true|string|
|field_value|formData|Value of the map collection field.|true|string|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
204  

### DELETE /tmunit/`unit_id`/field/set

**Summary**  
Remove a single or all values from the given set collection field.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|field|formData|Name of the set collection field.|true|string|
|field_value|formData|Value of the set collection field.|false|string|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
204  

### POST /tmunit/`unit_id`/field/set

**Summary**  
Add a new value to the given set collection field.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|field|formData|Name of the set collection field.|true|string|
|field_value|formData|Value of the set collection field.|true|string|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
204  

### POST /tmunit/`unit_id`/index

**Summary**  
Reindex the TM unit.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`unit_id`|path|ID of the unit.|true|string (uuid)|

**Responses**  
200  

### POST /tmunit/`unit_id`/move

**Summary**  
Move the TM unit to a different vault.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|event_user_id|formData|ID of the user moving the TM unit.|false|string (uuid)|
|`unit_id`|path|ID of the unit.|true|string (uuid)|
|vault_id|formData|ID of the vault.|true|string (uuid)|

**Responses**  
200  

### GET /vault/`vault_id`/count/locale/pair

**Summary**  
List all locale pairs and their total counts of TM units in the vault.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`vault_id`|path|ID of the vault.|true|string (uuid)|

**Responses**  
200  

### GET /vault/`vault_id`/count/tmunit

**Summary**  
Get the total count of all TM units in the vault.

**Parameters**  

|Name|Located In|Description|Required|Type
|:---|:---|:---|:---|:---|
|`vault_id`|path|ID of the vault.|true|string (uuid)|

**Responses**  
200  

## Appendix

### Comparison Types
Below is the list of comparison types for filtering and penalizing matches.

|Type|Notes|
|:---|:---|
|`equals`|For a String or UUID value, this is the same as using the `equals()` method in java.  For a number value, this is the same as a using relational operators.|
|`not_equals`|Performs the 'equals' comparison and then reverses it.|
|`greater_than`|For a String or UUID value, this is the same as using the `compareTo()` method in java.  For a number value, this is the same as a using relational operators.|
|`greater_than_or_equals`|For a String or UUID value, this is the same as using the `compareTo()` method in java.  For a number value, this is the same as a using relational operators.|
|`less_than`|For a String or UUID value, this is the same as using the `compareTo()` method in java.  For a number value, this is the same as a using relational operators.|
|`less_than_or_equals`|For a String or UUID value, this is the same as using the `compareTo()` method in java.  For a number value, this is the same as a using relational operators.|
|`contains`|For a String value, this is the same as using the `contains()` method in java.  For a Set or List value, this checks if it contains the element.  For a Map value, this checks if it contains the key.|
|`not_contains`|Performs the 'contains' comparison and then reverses it.|
|`empty`|For boolean values, this checks if it is false.  For a number value, this checks if it is 0.  For both String values and collection values, this is the same as using the `isEmpty()` method in java.  Also, if the value is `None`, `null`, etc. then it is considered to be empty.|
|`not_empty`|Performs the 'empty' comparison and then reverses it.|

### Custom Field Types
Below is the list of custom field types for custom fields on a TM unit.  Only single primitive values are supported for custom fields (i.e. the custom field itself cannot be a set or map).  By default custom fields are the `text` type.

|Type|Notes|
|:---|:---|
|`boolean`|The same as the `boolean` type in java.|
|`int`|The same as the `int` type in java.|
|`float`|The same as the `float` type in java.|
|`text`|The same as the `String` type in java.|

### Match Types
Below is the list of match types for TM search.  They define the overall categories of the match strength.  The types are integers so that match results can be sorted by match type.  The match percentages (i.e. the match score) are separate from the match type.

|Type|Description|
|:---|:---|
|`1`|Context 100% - Formatted With String ID|
|`2`|Context 100% - Formatted With Previous Current Next|
|`3`|Context 100% - Formatted With Previous Current|
|`4`|Context 100% - Formatted With Current Next|
|`5`|Context 100% - Plain Text With String ID|
|`6`|Context 100% - Plain Text With Previous Current Next|
|`7`|Context 100% - Plain Text With Previous Current|
|`8`|Context 100% - Plain Text With Current Next|
|`9`|Exact 100%|
|`10`|Syntax 100%|
|`11`|Fuzzy|
|`12`|Concordance|

### Sort Directions
Below is the list of sort directions for TM search.

|Direction|Description|
|:---|:---|
|`asc`|Ascending|
|`desc`|Descending|
