package com.lingotek.system.workflow.api.camunda

import java.beans.FeatureDescriptor
import java.util

import com.google.inject.{Binding, Injector, Key}
import org.camunda.bpm.engine.ProcessEngineException
import org.camunda.bpm.engine.impl.javax.el.{ELContext, ELResolver}
import play.api.Logger

import scala.collection.mutable

/**
  * @author Allan G. Ramirez (aramirez@lingotek.com)
  */
class GuiceElResolver(injector: Injector) extends ELResolver {
  val bindings: util.Map[Key[_], Binding[_]] = injector.getAllBindings
  val uniqBeanBindingMap: mutable.Map[String, Key[_]] = new mutable.HashMap()
  val duplBeanNames = new mutable.HashSet[String]

  /*
    Unlike spring, google guice map instances against the fully qualified name of the class.
    Eg. com.sample.CloudX -> new com.sample.CloudX(). Since camunda bean naming in expressions follow Spring's convention,
    we need to adopt to it.
    One of Spring's convention in bean naming is - retrieve the simple class name of interface then make the first letter
    lower cased. Eg, the bean name for com.sample.MyInterface is myInterface.
   */
  val entrySetIt: util.Iterator[util.Map.Entry[Key[_], com.google.inject.Binding[_]]] = bindings.entrySet().iterator()
  while (entrySetIt.hasNext) {
    val entry = entrySetIt.next()
    val key = entry.getKey
    val typeName = key.getTypeLiteral.getType.getTypeName
    if (typeName.toLowerCase.indexOf("workflowservice") > -1) {
      Logger.info("Found Workflow Service")
    }
    // typeName is usually the class' fully qualified name. We'll extract the class name by removing package name
    val className = typeName.substring(typeName.lastIndexOf('.') + 1)
    uniqBeanBindingMap.put(typeName, key)
    if (typeName != className) {
      val c: Array[Char] = className.toCharArray
      c(0) = Character.toLowerCase(c(0))
      val beanName = c.mkString
      if (uniqBeanBindingMap.contains(beanName)) {
        duplBeanNames.add(beanName)
      } else {
        uniqBeanBindingMap.put(beanName, key)
      }
    }
  }
  if (duplBeanNames.nonEmpty) {
    Logger.warn("These bean names will not be resolved if used in camunda expression since multiple instances are found: "
      + duplBeanNames.mkString(", "))
    for (key <- duplBeanNames) {
      uniqBeanBindingMap.remove(key)
    }
  }

  override def setValue(context: ELContext, base: scala.Any, property: scala.Any, value: scala.Any): Unit = {
    val key = property match {
      case property: String => property
      case _ => property.toString
    }
    if (uniqBeanBindingMap.contains(key)) {
      throw new ProcessEngineException("Cannot set value of '" + property +
        "', it resolves to a bean defined in the guice container.")
    }
  }

  override def getValue(context: ELContext, base: scala.Any, property: scala.Any): AnyRef = {
    if (!Option(base).isDefined) {
      val key = property match {
        case property: String => property
        case _ => property.toString
      }
      val optKeyObj = uniqBeanBindingMap.get(key)
      if (optKeyObj.isDefined) {
        context.setPropertyResolved(true)
        injector.getInstance(optKeyObj.get).asInstanceOf[AnyRef]
      } else {
        None
      }
    } else {
      None
    }
  }

  override def getType(context: ELContext, base: scala.Any, property: scala.Any): Class[_] = classOf[AnyRef]

  override def getFeatureDescriptors(context: ELContext, base: scala.Any): util.Iterator[FeatureDescriptor] =
    util.Collections.emptyIterator()

  override def getCommonPropertyType(context: ELContext, base: scala.Any): Class[_] = classOf[AnyRef]

  override def isReadOnly(context: ELContext, base: scala.Any, property: scala.Any): Boolean = true
}
