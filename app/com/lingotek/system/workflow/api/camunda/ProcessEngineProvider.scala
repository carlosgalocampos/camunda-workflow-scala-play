package com.lingotek.system.workflow.api.camunda

import javax.inject.Provider

import com.google.inject.{Inject, Injector, Singleton}
import com.lingotek.system.workflow.api.environment.Environment
import org.camunda.bpm.engine.{ProcessEngine, ProcessEngineConfiguration}
import org.camunda.bpm.engine.impl.cfg.{ProcessEngineConfigurationImpl, StandaloneProcessEngineConfiguration}
import org.camunda.bpm.engine.impl.el.ExpressionManager
import org.camunda.bpm.engine.impl.javax.el.{CompositeELResolver, ELResolver}
import play.api.inject.ApplicationLifecycle
import play.api.{Configuration, Logger}

import scala.concurrent.Future

/**
  * @author Allan G. Ramirez (aramirez@lingotek.com)
  */
@Singleton
class ProcessEngineProvider @Inject()(protected val config: Configuration, protected val lifecycle: ApplicationLifecycle,
                                      protected val injector: Injector, protected val environment: Environment) extends Provider[ProcessEngine] {
  private val bpmnName = config.getString("camunda.bpmn.name").getOrElse("")
  private val bpmnSource = config.getString("camunda.bpmn.source").getOrElse("")
  private val postgresUrl = environment.postgresUrl
  private val postgresPort = environment.postgresPort
  private val postgresCamundaDB = environment.postgresCamundaDB
  private val postgresDriver = environment.postgresDriver
  private val postgresUsername = environment.postgresUsername
  private val postgresPassword = environment.postgresPassword
  private val camundaDbUrl = s"jdbc:postgresql://${postgresUrl}:${postgresPort}/${postgresCamundaDB}"


  private val expressionManager = new ExpressionManager() {
    override def createElResolver(): ELResolver = {
      val elResolver = super.createElResolver() match {
        case resolver: CompositeELResolver => resolver
        case _ => throw new ClassCastException
      }
      elResolver.add(new GuiceElResolver(injector))
      elResolver
    }
  }
  private val configuration: ProcessEngineConfigurationImpl = new StandaloneProcessEngineConfiguration()
    .setJdbcUrl(camundaDbUrl)
    .setJdbcDriver(postgresDriver)
    .setJdbcUsername(postgresUsername)
    .setJdbcPassword(postgresPassword)
    .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE)
    .setHistory(ProcessEngineConfiguration.HISTORY_FULL)
    .setJobExecutorActivate(true)
    .setExpressionManager(expressionManager)

  Logger.info("Starting process engine...")
  private val engine = configuration.buildProcessEngine()
  lifecycle.addStopHook { () =>
    Logger.info("Closing process engine...")
    Future.successful(engine.close())
  }

  private val bpmnClasspathResourcesOpt = config.getConfig("camunda.bpmn.classpathResources")
  if (bpmnClasspathResourcesOpt.isDefined) {
    Logger.info("Deploying process definition...")
    val deployment = engine.getRepositoryService.createDeployment()
      .enableDuplicateFiltering(true)
      .name(bpmnName)
      .source(bpmnSource)

    bpmnClasspathResourcesOpt.get.entrySet.filter(config => {
      config._1.endsWith(".file")
    }).foreach(config => {
      val value = config._2.unwrapped().toString
      deployment.addClasspathResource(value)
    })

    deployment.deploy()
  } else {
    Logger.info("No process definition to deploy...")
  }

  override def get(): ProcessEngine = {
    engine
  }

  Logger.debug("ZooKeeper overrides")
  Logger.debug(s"Zookeeper ${config.getString(environment.Key.POSTGRES_URL)} - $camundaDbUrl")
  Logger.debug(s"Zookeeper ${config.getString(environment.Key.POSTGRES_USERNAME)} - $postgresUsername")
  Logger.debug(s"Zookeeper ${config.getString(environment.Key.POSTGRES_PASSWORD)} - $postgresPassword")
  Logger.debug(s"Zookeeper ${config.getString(environment.Key.POSTGRES_CAMUNDA_DB)} - $postgresCamundaDB")
}
