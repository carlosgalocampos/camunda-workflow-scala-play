package com.lingotek.system.workflow.api.modules

import java.nio.file.{Files, Paths}

import com.lingotek.system.workflow.api.environment.Environment
import com.typesafe.config.ConfigFactory
import play.api.{Configuration, _}
import play.api.db.slick.SlickModule
import play.api.inject.guice.{GuiceApplicationBuilder, GuiceApplicationLoader}

class CustomApplicationLoader(initialBuilder: GuiceApplicationBuilder = new GuiceApplicationBuilder) extends GuiceApplicationLoader(initialBuilder) {
  def this() = this(new GuiceApplicationBuilder)

  private def getAppModeConf(env: play.api.Environment): String = {
    val mode = env.mode
    val appConf = s"application.${mode.toString.toLowerCase}.conf"
    val url = Option(env.classLoader.getResource(appConf))
    if (url.isDefined && Files.exists(Paths.get(url.get.getPath))) appConf else "application"
  }

  private def loadAppConfig(env: play.api.Environment, context: ApplicationLoader.Context): Configuration = {

    val appConfFile = getAppModeConf(env)

    Logger.info("Using app conf file: " + appConfFile)
    val applicationConfig = Configuration(ConfigFactory.load(appConfFile))
    context.initialConfiguration ++ applicationConfig
  }

  override def builder(context: ApplicationLoader.Context): GuiceApplicationBuilder = {
    val env: play.api.Environment = context.environment

    initialBuilder
      .in(env)
      .loadConfig(loadAppConfig(env, context))
      .overrides(overrides(context): _*)
  }
}
