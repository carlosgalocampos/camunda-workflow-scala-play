package com.lingotek.system.workflow.api.modules

import com.google.inject.AbstractModule
import com.lingotek.system.workflow.api.camunda.ProcessEngineProvider
import org.camunda.bpm.engine.ProcessEngine
import play.api.libs.concurrent.AkkaGuiceSupport


/**
 * @author Allan G. Ramirez (aramirez@lingotek.com)
 */
class AppBindings extends AbstractModule with AkkaGuiceSupport {
  override def configure(): Unit = {
    bind(classOf[ProcessEngine]).toProvider(classOf[ProcessEngineProvider]).asEagerSingleton()
  }
}
