package com.lingotek.system.workflow.api.play.controllers

import javax.inject.Inject

import com.google.inject.Singleton
import com.lingotek.system.workflow.api.play.services.DefaultWorkflowService
import play.api.mvc.{Action, Controller}

@Singleton
class DefaultWorkflowController @Inject()(service: DefaultWorkflowService) extends Controller {

  def doDummyOperation = Action { implicit request =>
    service.startWorkflowProcess
    Ok
  }
}
