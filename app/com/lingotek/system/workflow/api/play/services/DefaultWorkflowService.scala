package com.lingotek.system.workflow.api.play.services

import java.util
import javax.inject.Inject

import com.google.inject.Singleton
import org.camunda.bpm.engine.ProcessEngine
import play.api.{Configuration, Logger}

@Singleton
class DefaultWorkflowService @Inject()(protected val processEngine: ProcessEngine, protected val config: Configuration) {

  /**
    * Start a workflow by creating a process instance for the particular instance.
    */
  def startWorkflowProcess: Unit = {
    val variables = new util.HashMap[String, Object]
    variables.put("arg1", "argument")
    variables.put("arg2", new Integer(1))
    val defaultProcessInstanceKey = config.getString("camunda.bpmn.classpathResources.default.id").get
    val processInstance = processEngine.getRuntimeService.startProcessInstanceByKey(defaultProcessInstanceKey, variables)

    val task = processEngine.getTaskService.createTaskQuery.processInstanceId(processInstance.getId)
      .taskDefinitionKey("task_do_dummy_operation").singleResult

    processEngine.getTaskService.complete(task.getId)
  }

  /**
    * The delegate method invoke by the process engine.
    *
    * @param arg1 The first argument.
    * @param arg2 The second argument.
    * @return
    */
  def doDummyOperation(arg1: String, arg2: Int): Unit =
    Logger.info(s"This is a dummy content of String $arg1 and Integer $arg2")
}
