package com.lingotek.system.workflow.api.environment

import javax.inject.{Inject, Singleton}

import com.lingotek.configuration.zookeeper
import com.lingotek.configuration.zookeeper.ZKCuratorFactory
import play.api.Configuration

@Singleton
class Environment @Inject()(config: Configuration) {

  object Key {
    private val PROPERTY = "zookeeper.property"
    private val POSTGRES = s"$PROPERTY.postgres"
    val POSTGRES_URL = s"$POSTGRES.url.key"
    val POSTGRES_PORT = s"$POSTGRES.port.key"
    val POSTGRES_CAMUNDA_DB = s"$POSTGRES.camunda.db.key"
    val POSTGRES_USERNAME = s"$POSTGRES.username.key"
    val POSTGRES_PASSWORD = s"$POSTGRES.password.key"
    val POSTGRES_CONNECTION_POOL_SIZE = s"$POSTGRES.connection.pool.key"
    val POSTGRES_CONNECTION_TIMEOUT = s"$POSTGRES.connection.timeout.key"
    val POSTGRES_CONNECTION_VALIDATION_TIMEOUT = s"$POSTGRES.connection.validation.timeout.key"
  }

  object Default {
    private val PROPERTY = "zookeeper.property"
    private val POSTGRES = s"$PROPERTY.postgres"
    val POSTGRES_URL = s"$POSTGRES.url.default"
    val POSTGRES_PORT = s"$POSTGRES.port.default"
    val POSTGRES_USERNAME = s"$POSTGRES.username.default"
    val POSTGRES_PASSWORD = s"$POSTGRES.password.default"
    val POSTGRES_CAMUNDA_DB = s"$POSTGRES.camunda.db.default"
    val POSTGRES_DRIVER = s"$POSTGRES.driver.default"
    val SLICK_POSTGRES_DRIVER = s"$POSTGRES.slick.driver.default"
    val POSTGRES_CONNECTION_POOL_SIZE = s"$POSTGRES.connection.pool.default"
    val POSTGRES_CONNECTION_TIMEOUT = s"$POSTGRES.connection.timeout.default"
    val POSTGRES_CONNECTION_VALIDATION_TIMEOUT = s"$POSTGRES.connection.validation.timeout.default"

  }

  def zookeeperUrl: String = ZKCuratorFactory.getZkConnectionString

  private def getFromZookeeper(key: String, default: String): String =
    zookeeper.Configuration.get(config.getString(key).get, config.getString(default).get)

  def postgresUrl: String = getFromZookeeper(Key.POSTGRES_URL, Default.POSTGRES_URL)

  def postgresPort: String = getFromZookeeper(Key.POSTGRES_PORT, Default.POSTGRES_PORT)

  def postgresUsername: String = getFromZookeeper(Key.POSTGRES_USERNAME, Default.POSTGRES_USERNAME)

  def postgresPassword: String = getFromZookeeper(Key.POSTGRES_PASSWORD, Default.POSTGRES_PASSWORD)

  def postgresCamundaDB: String = getFromZookeeper(Key.POSTGRES_CAMUNDA_DB, Default.POSTGRES_CAMUNDA_DB)

  def postgresDriver: String = config.getString(Default.POSTGRES_DRIVER).get

  def slickPostgresDriver: String = config.getString(Default.SLICK_POSTGRES_DRIVER).get

  def postgresConnectionPoolSize: String = getFromZookeeper(Key.POSTGRES_CONNECTION_POOL_SIZE, Default.POSTGRES_CONNECTION_POOL_SIZE)

  def postgresConnectionTimeout: String = getFromZookeeper(Key.POSTGRES_CONNECTION_TIMEOUT, Default.POSTGRES_CONNECTION_TIMEOUT)

  def postgresConnectionValidationTimeout: String = getFromZookeeper(Key.POSTGRES_CONNECTION_VALIDATION_TIMEOUT, Default.POSTGRES_CONNECTION_VALIDATION_TIMEOUT)

}
